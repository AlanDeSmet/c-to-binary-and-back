all: 5-final-hex.txt 5-final-binary.txt

validate: 4a-objdump.s 2-trimmed-assembly.s
	paste $^

clean:
	rm -f *.s  *.bin *.tmp *-final-*.txt

5-final-hex.txt: 4-trimmed.bin
	xxd -p -c256 "$<" > "$@"

5-final-binary.txt: 4-trimmed.bin
	xxd -b -c1 "$<"  | cut -d" " -f2 | tr -d "\n" > "$@"


4a-objdump.s: 4-trimmed.bin
	objdump -b binary -D -m i386:x86-64 "$<" | expand | sed -n -e '/0:/,$$ {p}' > "$@"
	@test -s "$@"

4-trimmed.bin: 3-full-binary.bin 3a-start.tmp 3b-length.tmp
	dd skip="$$(cat 3a-start.tmp)" count="$$(cat 3b-length.tmp)" if="$<" of="$@" bs=1 status=noxfer
	@test -s "$@"

3a-start.tmp: 3-full-binary.bin
	(echo "ibase=16; "; objdump -h "$<" | grep text | awk '{print $$6}' | sed 's/^0*//') | bc > "$@"
	@test -s "$@"
3b-length.tmp: 3-full-binary.bin
	(echo "ibase=16; "; objdump -h "$<" | grep text | awk '{print $$3}' | sed 's/^0*//') | bc > "$@"
	@test -s "$@"


3-full-binary.bin: 2-trimmed-assembly.s
	as "$<" -o "$@"
	@test -s "$@"

# Trim to just parts we're responsible for
2-trimmed-assembly.s: 1-verbose-assembly.s
	sed -n -e 's/  *#[, ].*//' -e '/^# 0-C.c/,/^# 0-C.c:7: }/ {/^# /d;p}' -e '' "$<" > "$@"
	@test -s "$@"

1-verbose-assembly.s: 0-C.c
	gcc -S -fverbose-asm "$<" -o- | expand > "$@"
	@test -s "$@"

