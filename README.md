A simple example showing from compiler to assembly to binary, with a brief stop
at disassembly.

This is just a little worked example I did for this Twitter thread:
https://twitter.com/AlanDeSmet/status/1432788718784552963

Use `make` to generate the various steps. Use `make validate` to see a
comparison between the assembly and the disassemlbed binary; they should be
functionally the same, but will vary in irrelevant details (like using
hexadecimal ver decimal values or whitespace).

- 0-C.c - The starting file, C source code.
- 1-verbose-assembly.s - The C compiled to assembly.
- 2-trimmed-assembly.s - The assembly trimmed to just the bit that corresponds to our code. The rest is boilerplate.
- 3-full-binary.bin - The binary assembled from 2-trimmed-assembly.s.
- 3a-start.tmp and 3b-length.tmp - working files noting where "our" code is in the binary file.
- 4-trimmed.bin - The binary file trimmed to just the bit htat corresponds to our code. The rest is boilerplate.
- 5-final-binary.txt - The trimmed binary converted to 0 and 1 characters.
- 5-final-hex.txt - The trimmed binary converted to hexadecimal characters

Assumes a reasonably well equipped Linux-like system with:

- GNU Make
- as
- awk
- bc
- dd
- echo
- gcc
- grep
- objdump
- paste
- rm
- sed
- test
- xxd



